# jasperstarter-env - The official JasperStarter Build Environment

JasperStarter is an opensource command line launcher and batch compiler for
[JasperReports][].

The official homepage is [jasperstater.cenote.de][].

## About this image

Based on ubuntu:20.04

This Docker image is used to build [JasperStarter] including windows-setup.
It does not contain more installed programmes but it's .m2 repo is preloaded
with almost all needed dependencies for speed up the build and save bitbucket
pipeline build minutes.




## How to build JasperStarter with Docker

You need the following programms installed on your local machine:

* docker/podman
* git

Assuming your are on linux, your username is _peter_ and you put sourcecode in
a directory _src_ under your home.

```bash
cd ~/src
git clone https://bitbucket.org/cenote/jasperstarter.git
docker run -it --volume=/home/peter/src/jasperstarter:/jasperstarter --workdir=/jasperstarter cenote/jasperstarter-env
```


Now you can build a snapshot

```bash
mvn clean package -P snapshot,windows-setup
```

or a release

```bash
git checkout JasperStarter-3.0.0
mvn clean package -P release,windows-setup
```

After you exit from your new created container you can just restart the same container.

List your container:

```bash
docker ps -l
CONTAINER ID        IMAGE                      COMMAND        CREATED             STATUS                      PORTS               NAMES
51e8a151b77f        cenote/jasperstarter-env   "/bin/bash"    28 minutes ago      Exited (0) 10 minutes ago                       kickass_franklin
```

Start your container:

```bash
docker start -ai kickass_franklin
```


[jasperstater.cenote.de]:http://jasperstarter.cenote.de/
[JasperReports]:http://community.jaspersoft.com/project/jasperreports-library
[JasperStarter]:http://jasperstarter.cenote.de/



Installed software:

* ca-certificates
* curl
* fontconfig (making ttf-mscorefonts available to java)
* git
* lib32ncurses6 (needed for launch4j-maven-plugin)
* lib32z1 (needed for launch4j-maven-plugin)
* libstdc++6:i386 (needed for nsis)
* nsis-2.46
* openssh-client
* software-properties-common
* ttf-mscorefonts-installer
* wget
* openjdk-8
* ant
* maven
* almost all maven dependencies
