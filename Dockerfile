FROM ubuntu:20.04

RUN set -e && \
    apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# ttf-mscorefonts-installer has often problems with font download. Checking this first and exit on fail
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections && \
    apt-get update && apt-get install -y --no-install-recommends ttf-mscorefonts-installer 2>&1 | tee fontinstall.log |cat && \
    if grep -q Traceback fontinstall.log ; then echo "Error on font download!" ; exit 1; fi && rm fontinstall.log

# Basic things
RUN set -e && \
    ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    fontconfig \
    git \
    openssh-client \
    software-properties-common \
    wget \
    openjdk-8-jdk \
    maven \
    ant \
    && update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java \
    && rm -rf /var/lib/apt/lists/*

# nsis 
RUN set -e && \
    apt-get update && apt-get install -y --no-install-recommends \
    nsis \
    && rm -rf /var/lib/apt/lists/*
    

# JasperStarter Maven dependencies
# Download (almost) all dependencies for building JasperStarter to
# speed up Buildtime and save Bitbucket Pipeline minutes.

## A simple solution could be this but it gets not all.
#ADD https://bitbucket.org/cenote/jasperstarter/raw/master/pom.xml .
#RUN mvn dependency:go-offline && rm pom.xml

## this needs a git checkout of Jasperstarter:
#RUN mvn dependency:analyze-report

## if we need a checkout anyway, just make a full release build.
RUN cd /tmp && \
    git clone https://bitbucket.org/cenote/jasperstarter.git && \
    cd jasperstarter && \
    mvn package -P release,windows-setup && \
    rm -R /tmp/jasperstarter


